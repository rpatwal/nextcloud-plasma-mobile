set(sharefiles_SRCS
    main.cpp
    webview/signin.cpp
    )

qt5_add_resources(RESOURCES resources.qrc)
add_executable(sharefiles ${sharefiles_SRCS} ${RESOURCES})
target_link_libraries(sharefiles Qt5::Core  Qt5::Qml Qt5::Quick Qt5::Svg Qt5::WebEngine Qt5::WebEngineWidgets)
install(TARGETS sharefiles ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})
