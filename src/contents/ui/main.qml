import QtQuick 2.1
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import QtWebEngine 1.0

Kirigami.ApplicationWindow {
    id: root

    title: "Sharefiles"

    width: 360
    height: 600

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            id: pane
            title: "Sharefiles"
            background: Rectangle {
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                color: Kirigami.Theme.backgroundColor
            }

            ColumnLayout {
                spacing: 10
                anchors.fill: parent

                Item { Layout.fillHeight: true }

                TextField {
                    id: textfield
                    placeholderText: qsTr("Server address https://...")
                    Layout.alignment: Qt.AlignCenter
                    Layout.maximumWidth: Kirigami.Units.gridUnit * 20
                    Layout.fillWidth: true
                }

                ToolButton {
                    icon.name: "plasma-browser-integration"
                    icon.color: "black"
                    icon.width: 120
                    icon.height: 100
                    text:"login"
                    display: ToolButton.TextUnderIcon
                    onClicked:{
                        console.log(textfield.text);
                        signin.openWebView(textfield.text);
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                Item { Layout.fillHeight: true }
            }
        }
    }
}
