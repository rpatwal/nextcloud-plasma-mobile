#include <QtWidgets>
#include <QWebEngineView>
#include <QWebEnginePage> 
#include <QWebEngineHttpRequest> 

class SignIn : public QObject{
    Q_OBJECT
    QWebEngineHttpRequest request;
    QWebEngineView * view = new QWebEngineView;
    bool isSigninDone = false;

public:
    SignIn();
    ~SignIn();

public slots:
    void openWebView(QString urlString);
    void finalUrlHandler(const QUrl &url);
};